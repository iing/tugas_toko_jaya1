<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";

    public function tampilDataPembelian()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
    }

    public function savePembelianHeader()
    {
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['kode_supplier']  = $this->input->post('kode_supplier');
        $data['tanggal']        = date('Y-m-d');
        $data['approved']       = 1;
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }

    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_pembelian_h;
        }

        return $last_id;
    }

    public function tampilDataPembelianDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
        );  
        return $query->result();	
    }

    public function savePembelianDetail($id)
    {
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');

        $data['id_pembelian_h'] = $id;
        $data['kode_barang']    = $this->input->post('kode_barang');
        $data['qty']            = $qty;
        $data['harga']          = $harga;
        $data['jumlah']         = $qty + $harga;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }


    public function rules()
    
    {
        return [
        [
        'field' => 'no_transaksi',
        'label' => 'no transaksi',
        'rules' => 'required|max_length[5]',
        'errors' => [
           'required' => 'no transaksi tidak boleh kosong.',
           'max_length' => 'no transaksi tidak boleh lebih dari 5 karakter.',
           ]

           ],
           [

         'field' => 'kode_supplier',
         'label' => 'kode supplier',
         'rules' => 'required',
         'errors' => [
           'required' => 'kode supplier tidak boleh kosong.',
            
                   ]
           ]
           ];
         }

         
     public function rules1()
    
    {
        return [
        [
        'field' => 'kode_barang',
        'label' => 'kode barang',
        'rules' => 'required|max_length[5]',
        'errors' => [
           'required' => 'kode barang tidak boleh kosong.',
           'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
           ]
           ],
           [
         'field' => 'qty',
         'label' => 'qty',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'qty tidak boleh kosong.',
           'numeric' => 'qty harus angka.',
            ]
           ],
           [
             'field' => 'harga',
         'label' => 'harga',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'harga tidak boleh kosong.',
           'numeric' => 'harga harus angka.',
            
                   ]
           ]
           ];
           


    }

    public function tampilDataPembelianpagination($perpage, $uri, $data_pencarian)
  {
    $this->db->select('*');
    if (!empty($data_pencarian)) {
      $this->db->like('no_transaksi', $data_pencarian);
    }
    $this->db->order_by('id_pembelian_h', 'asc');
    
    $get_data = $this->db->get($this->_table_header, $perpage, $uri);
    if ($get_data->num_rows() > 0) {
      return $get_data->result();
    } else {
      return null;
    }
    
    }
    
  
  public function tombolpagination($data_pencarian)
  {
    $this->db->like('no_transaksi', $data_pencarian);
    $this->db->from($this->_table_header);
    $hasil = $this->db->count_all_results();
    
    $pagination['base_url'] = base_url(). 'pembelian/listpembelian/load/';
    $pagination['total_rows'] = $hasil;
    $pagination['per_page'] = "2";
    $pagination['uri_segment'] = 4;
    $pagination['num_links'] = 2;
    
    // custom paging configuration
    $pagination['full_tag_open'] = '<div class="pagination">';
    $pagination['full_tag_close'] = '</div>';
    
    $pagination['first_link'] = 'first page';
    $pagination['first_tag_open'] = '<span class="firstlink">';
    $pagination['first_tag_close'] = '</span>';
    
    $pagination['last_link'] = 'last page';
    $pagination['last_tag_open'] = '<span class="lastlink">';
    $pagination['last_tag_close'] = '</span>';
    
    $pagination['next_link'] = 'next page';
    $pagination['next_tag_open'] = '<span class="nextlink">';
    $pagination['next_tag_close'] = '</span>';
    
    $pagination['prev_link'] = 'prev page';
    $pagination['prev_tag_open'] = '<span class="prevlink">';
    $pagination['prev_tag_close'] = '</span>';
    
  
    $pagination['cur_tag_open'] = '<span class="curlink">';
    $pagination['cur_tag_close'] = '</span>';
    
    $pagination['num_tag_open'] = '<span class="numlink">';
    $pagination['num_tag_close'] = '</span>';
    
    $this->pagination->initialize($pagination);
    $hasil_pagination = $this->tampilDataPembelianpagination($pagination['per_page'],
    $this->uri->segment(4), $data_pencarian);
    
    return $hasil_pagination;
  }
   public function tampilreportPembelian()
    {
        
           $this->db->select('barang.stok, pembelian_detail.qty, pembelian_detail.jumlah, pembelian_header.tanggal, pembelian_header.no_transaksi, pembelian_header.id_pembelian_h');
            $this->db->from('pembelian_header');
            $this->db->join('pembelian_detail', 'pembelian_detail.id_pembelian_d = pembelian_header.id_pembelian_h');
            $this->db->join('barang', 'pembelian_detail.kode_barang = barang.kode_barang');
             $query = $this->db->get();  
          return $query->result();
      
    }
	

}